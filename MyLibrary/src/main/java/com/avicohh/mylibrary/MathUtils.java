package com.avicohh.mylibrary;

import com.avicohh.myinnerlibrary.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MathUtils {
    public static String plus(int a, int b) {
        List<String> listOfStrings = ListUtils.getListOfStrings(new ArrayList<Object>() {{
            add(a);
            add("+");
            add(b);
        }});
        String result = Arrays.toString(listOfStrings.toArray()) + " = " + (a + b);
        return result;
    }
}

package com.avicohh.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.avicohh.mylibrary.MathUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonCalculate = (Button) findViewById(R.id.calculate);
        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextA = (EditText) findViewById(R.id.number_a);
                int numberA = Integer.valueOf(editTextA.getText().toString());
                EditText editTextB = (EditText) findViewById(R.id.number_b);
                int numberB = Integer.valueOf(editTextB.getText().toString());
                TextView textViewResult = (TextView) findViewById(R.id.result);
                textViewResult.setText(MathUtils.plus(numberA, numberB));
            }
        });
    }
}

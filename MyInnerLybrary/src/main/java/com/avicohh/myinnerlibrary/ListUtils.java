package com.avicohh.myinnerlibrary;

import java.util.List;
import java.util.stream.Collectors;

public class ListUtils {
    public static List<String> getListOfStrings(List<Object> list) {
        return list.stream().map(o -> o.toString()).collect(Collectors.toList());
    }
}
